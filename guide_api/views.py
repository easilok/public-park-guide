from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
from django.db.models import Prefetch
from django.core.serializers import serialize  # import serializer from django 
from django.core.exceptions import PermissionDenied
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from guide.models import Zone, ZoneTranslations, Language, Park, \
        ParkTranslations, Life, LifeTranslations, LifeZone, ImageLifeTranslations, \
        ImageLife, ImageZone, ImageZoneTranslations
from .serializers import ZoneTranslationSerializer, ParkTranslationSerializer, \
        LifeTranslationSerializer, LifeZoneSerializer, LifeSerializer

class ZoneListSet(viewsets.ModelViewSet):
    """
    API endpoint that shows all zones
    """
    queryset = ZoneTranslations.objects.filter(language=Language.objects.first())
    serializer_class = ZoneTranslationSerializer

    def get_queryset(self):
        req = self.request
        lang = req.query_params.get('lang')
        l = Language.objects.first()
        if lang:
            l = Language.objects.filter(slug=lang).first()
        self.queryset = ZoneTranslations.objects.filter(language=l).prefetch_related(
                Prefetch('zone__imagezone_set', queryset=ImageZone.objects.filter(enable=True)),
                Prefetch('zone__imagezone_set__imagezonetranslations_set', queryset=ImageZoneTranslations.objects.filter(language=l))
                )
        return self.queryset

    def create(self, request):
        raise PermissionDenied

    def update(self, request, pk=None):
        raise PermissionDenied

    def partial_update(self, request, pk=None):
        raise PermissionDenied

    def destroy(self, request, pk=None):
        raise PermissionDenied

class ParkListSet(viewsets.ModelViewSet):
    """
    API endpoint that shows all parks
    This is currently filtered for one item only
    """
    queryset = ParkTranslations.objects.filter(language=Language.objects.first())[:1]
    serializer_class = ParkTranslationSerializer

    def get_queryset(self):
        req = self.request
        lang = req.query_params.get('lang')
        if lang:
            l = Language.objects.filter(slug=lang).first()
            if l is not None:
                self.queryset = ParkTranslations.objects.filter(language=l)[:1]
            return self.queryset
        else:
            return self.queryset

    def create(self, request):
        raise PermissionDenied

    def update(self, request, pk=None):
        raise PermissionDenied

    def partial_update(self, request, pk=None):
        raise PermissionDenied

    def destroy(self, request, pk=None):
        raise PermissionDenied

class ZoneAPI(APIView):
    """
    API endpoint that shows all park life
    """
    serializer_class = ZoneTranslationSerializer
    # throttle_scope = "cars_app"

    def get(self, request, *args, **kwargs):
        try:
            lang = request.query_params.get('lang')
            l = Language.objects.first()
            if lang:
                l = Language.objects.filter(slug=lang).first()
            zones = ZoneTranslations.objects.filter(language=l).prefetch_related(
                    Prefetch('zone__imagezone_set', queryset=ImageZone.objects.filter(enable=True)),
                    Prefetch('zone__imagezone_set__imagezonetranslations_set', queryset=ImageZoneTranslations.objects.filter(language=l))
                    )
            serializer = ZoneTranslationSerializer(zones, many=True)
        except Exception as e:
            zones = []
            serializer = ZoneTranslationSerializer(zones)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        raise PermissionDenied

    def put(self, request, *args, **kwargs):
        raise PermissionDenied

    def patch(self, request, *args, **kwargs):
        raise PermissionDenied

    def destroy(self, request, *args, **kwargs):
        raise PermissionDenied

class LifeZoneAPI(APIView):
    """
    API endpoint that shows all zone life
    """
    serializer_class = LifeZoneSerializer
    # throttle_scope = "cars_app"

    def get(self, request, *args, pk = None, **kwargs):
        try:
            id = pk
            lang = request.query_params.get('lang')
            l = Language.objects.first()
            if lang:
                l = Language.objects.filter(slug=lang).first()
            lifes = LifeZone.objects.filter(zone=id, life__lifetranslations__language=l).prefetch_related(
                    Prefetch('life__lifetranslations_set', queryset=LifeTranslations.objects.filter(language=l)),
                    Prefetch('life__imagelife_set', queryset=ImageLife.objects.filter(enable=True)),
                    Prefetch('life__imagelife_set__imagelifetranslations_set', queryset=ImageLifeTranslations.objects.filter(language=l)),
                    )
            serializer = LifeZoneSerializer(lifes, many=True)
        except Exception as e:
            lifes = []
            serializer = LifeZoneSerializer(lifes)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        raise PermissionDenied

    def put(self, request, *args, **kwargs):
        raise PermissionDenied

    def patch(self, request, *args, **kwargs):
        raise PermissionDenied

    def destroy(self, request, *args, **kwargs):
        raise PermissionDenied


class LifeAPI(APIView):
    """
    API endpoint that shows a life
    """
    serializer_class = LifeSerializer
    # throttle_scope = "cars_app"

    def get(self, request, *args, pk = None, **kwargs):
        try:
            id = pk
            lang = request.query_params.get('lang')
            l = Language.objects.first()
            if lang:
                l = Language.objects.filter(slug=lang).first()
            lifes = Life.objects.filter(id=id).prefetch_related(
                    Prefetch('lifetranslations_set', queryset=LifeTranslations.objects.filter(language=l)),
                    Prefetch('imagelife_set', queryset=ImageLife.objects.filter(enable=True)),
                    Prefetch('imagelife_set__imagelifetranslations_set', queryset=ImageLifeTranslations.objects.filter(language=l)),
                    )
            serializer = LifeSerializer(lifes, many=True)
        except Exception as e:
            lifes = None
            serializer = LifeSerializer(lifes)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        raise PermissionDenied

    def put(self, request, *args, **kwargs):
        raise PermissionDenied

    def patch(self, request, *args, **kwargs):
        raise PermissionDenied

    def destroy(self, request, *args, **kwargs):
        raise PermissionDenied

class LifeAPIRaw(View):
    """
    API using core django tools (not used at the moment)
    """
    def get(self, request, pk):
        try:
            id = pk
            lang = request.GET.get('lang')
            l = Language.objects.first()
            if lang:
                l = Language.objects.filter(slug=lang).first()
            lifes = LifeZone.objects.filter(zone=id, life__lifetranslations__language=l).prefetch_related(
                    Prefetch('life', queryset=Life.objects.all())
                    )
            # for lifeIndex in lifes.length:
            #     print(lifes[lifeIndex])
                # lifes[lifeIndex].life.lifetranslations_set.filter(language=l)
        except Exception as e:
            print("error raw api")
            print(e)
            lifes = []
        print(lifes)
        lifes_serialized = serialize('python', lifes, fields=['probability', 'proof', 'life'])
        return JsonResponse(lifes_serialized, safe=False)
