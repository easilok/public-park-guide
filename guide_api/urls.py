from django.urls import path, include
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'zone', views.ZoneListSet)
router.register(r'park', views.ParkListSet)

urlpatterns = [
        path(r'zone/<pk>/life', views.LifeZoneAPI.as_view()),
        path(r'life/<pk>', views.LifeAPI.as_view()),
        # path(r'zone', views.ZoneAPI.as_view()),
        # path(r'zone/<pk>/life', views.LifeAPIRaw.as_view()),
        path('', include(router.urls)),
]

