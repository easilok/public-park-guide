from rest_framework import serializers

from guide.models import Zone, ZoneTranslations, Park, ParkTranslations, \
        Life, LifeTranslations, LifeZone, ImageLife, ImageLifeTranslations, \
        ImageZone, ImageZoneTranslations

class ParkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Park
        fields = ['address', 'site', 'email', 'latitude', 'longitude']

class ParkTranslationSerializer(serializers.ModelSerializer):
    park = ParkSerializer(many = False)
    class Meta:
        model = ParkTranslations
        fields = ['name', 'welcome', 'park']

class ImageZoneTranslationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageZoneTranslations
        fields = ['name']

class ImageZoneSerializer(serializers.ModelSerializer):
    imagezonetranslations_set = ImageZoneTranslationsSerializer(many=True)
    path = serializers.SerializerMethodField()

    class Meta:
        model = ImageZone
        fields = [ 'imagezonetranslations_set', 'path', 'cover', 'order' ]

    def get_path(self, obj):
        if (obj.path):
            return obj.path.url

class ZoneSerializer(serializers.ModelSerializer):
    imagezone_set = ImageZoneSerializer(many=True)
    class Meta:
       model = Zone
       fields = ['id', 'code', 'latitude', 'longitude', \
               'initial', 'previous', 'next', 'imagezone_set']

class ZoneTranslationSerializer(serializers.ModelSerializer):
   zone = ZoneSerializer(many = False)
   audio = serializers.SerializerMethodField()
   class Meta:
       model = ZoneTranslations
       fields = ['zone', 'name', 'description', \
               'funFacts', 'audio']

   def get_audio(self, obj):
       if (obj.audio):
           return obj.audio.url

class ImageLifeTranslationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageLifeTranslations
        fields = [ 'name' ]

class ImageLifeSerializer(serializers.ModelSerializer):
    imagelifetranslations_set = ImageLifeTranslationsSerializer(many=True)
    class Meta:
        model = ImageLife
        fields = [ 'imagelifetranslations_set', 'path', 'order' ]

class LifeTranslationSerializer(serializers.ModelSerializer):
    # life = LifeSerializer(many = False)
    audio = serializers.SerializerMethodField()
    class Meta:
        model = LifeTranslations
        fields = ['name', 'life', 'description', 'funFacts', 'habitat', 'audio']

    def get_audio(self, obj):
        if (obj.audio):
            return obj.audio.url

class LifeSerializer(serializers.ModelSerializer):
    lifetranslations_set = LifeTranslationSerializer(many = True)
    imagelife_set = ImageLifeSerializer(many=True)
    class Meta:
        model = Life
        fields = ['id', 'scientific', 'code', 'location', 'group', 'lifetranslations_set', 'imagelife_set']

class LifeZoneSerializer(serializers.ModelSerializer):
    life = LifeSerializer(many = False)
    class Meta:
        model = LifeZone
        fields = ['life', 'probability', 'proof']
