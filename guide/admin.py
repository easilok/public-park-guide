from django.contrib import admin
from .models import Language, Park, ParkTranslations, Zone, \
        ZoneTranslations, ImageZone, ImageZoneTranslations, \
        ImageLife, ImageLifeTranslations, Life, LifeTranslations, \
        LifeZone

admin.site.register(Language)
admin.site.register(Park)
admin.site.register(ParkTranslations)
admin.site.register(Zone)
admin.site.register(ZoneTranslations)
admin.site.register(ImageZone)
admin.site.register(ImageZoneTranslations)
admin.site.register(ImageLife)
admin.site.register(ImageLifeTranslations)
admin.site.register(Life)
admin.site.register(LifeTranslations)
admin.site.register(LifeZone)
