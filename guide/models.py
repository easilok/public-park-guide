from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _

from .managers import CustomUserManager

class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)
    name = models.CharField(max_length=100)
    resetToken = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['password']

    objects = CustomUserManager()

    def __str__(self):
        return self.email


class Language(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.CharField(max_length=10, unique=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Park(models.Model):
    address = models.CharField(max_length=100)
    site = models.URLField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=6, default=0)
    longitude = models.DecimalField(max_digits=10, decimal_places=6, default=0)

    def __str__(self):
        return self.address

class ParkTranslations(models.Model):
    park = models.ForeignKey(Park, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    welcome = models.TextField()

    class Meta:
        unique_together = ['park', 'language']

    def __str__(self):
        return self.name

class Zone(models.Model):
    code = models.CharField(max_length=10, unique=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=6, default=0)
    longitude = models.DecimalField(max_digits=10, decimal_places=6, default=0)
    initial = models.BooleanField(default=False)
    previous = models.ForeignKey("Zone", related_name="previouszone", on_delete=models.SET_NULL, null=True, blank=True)
    next = models.ForeignKey("Zone", related_name="nextzone", on_delete=models.SET_NULL, null=True, blank=True)
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['initial', 'code']

    def __str__(self):
        return self.code

class ZoneTranslations(models.Model):
    zone = models.ForeignKey(Zone, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    audio = models.FileField(upload_to='zone/', null=True, blank=True)
    name = models.CharField(max_length=100)
    description = models.TextField()
    funFacts = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ['zone', 'language']
        ordering = ['-zone__initial', '-zone', '-name']

    def __str__(self):
        return self.name

class ImageZone(models.Model):
    zone = models.ForeignKey(Zone, on_delete=models.CASCADE)
    path = models.FileField(upload_to='zone/', unique=True)
    cover = models.BooleanField(default=False)
    enable = models.BooleanField(default=True)
    order = models.IntegerField(default=0)
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['order', 'created_at']

    def __str__(self):
        return self.path.name

class ImageZoneTranslations(models.Model):
    image = models.ForeignKey(ImageZone, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    class Meta:
        unique_together = ['image', 'language']

    def __str__(self):
        return self.name

class ImageLife(models.Model):
    lifeRef = models.ForeignKey('Life', on_delete=models.CASCADE)
    path = models.FileField(upload_to='life/', unique=True)
    enable = models.BooleanField(default=True)
    order = models.IntegerField(default=0)
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['order', 'created_at']

    def __str__(self):
        return self.path.name

class ImageLifeTranslations(models.Model):
    image = models.ForeignKey(ImageLife, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    class Meta:
        unique_together = ['image', 'language']

    def __str__(self):
        return self.name

class Life(models.Model):
    scientific = models.CharField(max_length=50)
    code = models.CharField(max_length=10, unique=True)
    location = models.ForeignKey(ImageLife, on_delete=models.SET_NULL, null=True, blank=True)
    LIFE_GROUP = (
        (0, 'fauna'),
        (1, 'flora'),
    )
    group = models.IntegerField(choices=LIFE_GROUP, default=0)
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['scientific', 'code']

    def __str__(self):
        return self.scientific + " (" + self.code + ")"

class LifeTranslations(models.Model):
    life = models.ForeignKey(Life, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    audio = models.FileField(upload_to='life/', null=True, blank=True)
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    funFacts = models.TextField(null=True, blank=True)
    habitat = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ['life', 'language']
        ordering = ['life', 'name']

    def __str__(self):
        return self.name

class LifeZone(models.Model):
    zone = models.ForeignKey(Zone, on_delete=models.CASCADE)
    life = models.ForeignKey(Life, on_delete=models.CASCADE)
    probability = models.IntegerField(default=0)
    proof = models.ForeignKey(ImageLife, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ['life', 'zone']

    def __str__(self):
        return str(self.zone) + " - " + str(self.life)
