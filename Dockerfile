# syntax=docker/dockerfile:1
FROM python:3

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt

COPY . /code/

RUN mkdir "/content"
RUN ln -sfr /content media

RUN mkdir "/database"
RUN ln -sfr /database database
VOLUME /database /content

EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
