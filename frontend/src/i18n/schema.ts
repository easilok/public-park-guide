import enUS from '../i18n/en-US.json';

// Type-define 'en-US' as the master schema for the resource
export type MessageSchema = typeof enUS

// define the number format schema
export type NumberSchema = {
  currency: {
    style: 'currency'
    currencyDisplay: 'symbol'
    currency: string
  }
}
