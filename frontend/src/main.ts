import { createApp } from 'vue';
import { createI18n } from 'vue-i18n';

import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';

import enUS from './i18n/en-US.json';
import ptPT from './i18n/pt-PT.json';

declare global {
  interface Window {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    gettext: any;
  }
}

import type { MessageSchema } from './i18n/schema';

const i18n = createI18n<[MessageSchema], 'en-US' | 'pt-PT'>({
  // locale: 'pt-PT',
  locale: 'en-US',
  fallbackLocale: 'en-US',
  messages: {
    'en-US': enUS,
    'pt-PT': ptPT
  }
});

const mainApp = createApp(App);
mainApp.use(store).use(router).use(i18n);

mainApp.mount('#app');

mainApp.provide('gettext', 'hello');
